INFO
=============
http://career-monster.appspot.com/

'Career Monster' is a search app intended for job seekers. The goal of the page is to make the users find the

job they are looking for in job database.

ElasticSearch as the back­end DB and search engine.


The Data
=============

The example data contains dummy vacancies. Each vacancy is represented by a document with the

following fields:

● jobtitle (string, mandatory): the job title.

● description (string, optional): a textual description of tasks and duties.

● languages (array of strings, optional): a list of required languages.

● compskills (array of strings, optional): a list of required computer skills.


The Idea of implementation
=============

According to the data for job vacancies there are fields that can be interpreted like Sets of values and not like plain text.
I had made decision to use every option except "jobtitle" & "description" as a variative component for the search.
This components of data proposed to the user in the form of selectable options/filters that can be added to the search query.
To collect the set of this options i had used aggregations.

I'd reserved scalability in the direction of additional indexes. There can be more additional option types except 'compskills' and 'languages'.
If any new index appear in DB then it would be added to the UI.

I had made the state of App to be build according to URL in case user refreshes the page or save/share link for some purposes.
The URL content has been designed to be as much friendly as it possible in prototype. It contains main query string and additional options that has been applied by user.
More/less readable/understandable about what the search was.

AngularJS has been used for UI. Great data binding and templating for the application dedicated to work with lists and sets. Clear app architecture. Unit testable and E2E testable.

No fancy UI until stable version of App with clear logic. Just function for first prototype.

Build has been done with use of GULP.


Futher ideas
=============

* Add autocomplete according to the vocab based on data in Vacancies
* Add "clear filter" controls to the screen
* Add bucket tool to make it possible to collect vacancies and review selection later.
* Add "share" buttons
* Add "remove" controls to hide particular results from search (can be used later to predict negative rules for user search queries)
* Add search history


PROJECT INITIALISATION
=============

* Install Node.js http://nodejs.org/
* npm cache clean
* npm install bower -g
* npm install yo -g
* npm install gulp -g
* npm install
* bower install
* gulp

RUN
=============
* Run Elasticsearch node on http://localhost:9200 (or use some other hosted Elasticsearch node)
* Bulk data in to it
* gulp watch


DEVELOPMENT
=============

* Live rebuild and browser refresh (plus Karma tests like Test Driven Development mode): "gulp watch"

* To create new Angular stuff use Yeoman generator (included): "yo angular:sub-generator [options]"
	more info about available sub-generators https://github.com/yeoman/generator-angular


BUILD ANGULAR APP FOR PRODUCTION
=============

* gulp
* RESULT: files in "dist" folder


CHANGES IN BOWER DEPENDENCIES
=============

* if new bower modules has been added then run: "gulp wiredep" and rebuild


TESTING
=============

* if want to run end-to-end test then run server with App and run: "gulp e2etests"
* Run Karma tests once: gulp test




