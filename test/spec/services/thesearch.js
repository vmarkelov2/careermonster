'use strict';

describe('Service: TheSearch', function () {

  // load the service's module
  beforeEach(module('careerMonsterApp'));

  // instantiate service
  var TheSearch;
  beforeEach(inject(function (_TheSearch_) {
    TheSearch = _TheSearch_;
  }));

  it('should do something', function () {
    expect(!!TheSearch).toBe(true);
  });

});
