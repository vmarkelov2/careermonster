'use strict';

describe('Service: fullTextFields', function () {

  // load the service's module
  beforeEach(module('careerMonsterApp'));

  // instantiate service
  var fullTextFields;
  beforeEach(inject(function (_fullTextFields_) {
    fullTextFields = _fullTextFields_;
  }));

  it('should do something', function () {
    expect(!!fullTextFields).toBe(true);
  });

});
