'use strict';

describe('Service: esClient', function () {

  // load the service's module
  beforeEach(module('careerMonsterApp'));

  // instantiate service
  var esClient;
  beforeEach(inject(function (_esClient_) {
    esClient = _esClient_;
  }));

  it('should do something', function () {
    expect(!!esClient).toBe(true);
  });

});
