'use strict';

describe('Service: Indexes', function () {

  // load the service's module
  beforeEach(module('careerMonsterApp'));

  // instantiate service
  var Indexes;
  beforeEach(inject(function (_Indexes_) {
    Indexes = _Indexes_;
  }));

  it('should do something', function () {
    expect(!!Indexes).toBe(true);
  });

});
