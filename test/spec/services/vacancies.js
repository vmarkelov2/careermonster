'use strict';

describe('Service: Vacancies', function () {

  // load the service's module
  beforeEach(module('careerMonsterApp'));

  // instantiate service
  var Vacancies;
  beforeEach(inject(function (_Vacancies_) {
    Vacancies = _Vacancies_;
  }));

  it('should do something', function () {
    expect(!!Vacancies).toBe(true);
  });

});
