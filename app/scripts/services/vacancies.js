'use strict';

/**
 * @ngdoc service
 * @name careerMonsterApp.Vacancies
 * @description
 * # Vacancies
 * Factory in the careerMonsterApp.
 */
angular.module('careerMonsterApp')
  .factory('Vacancies', ['$q', 'RESTbase', 'ELASTICprop', 'esClient', 'esFactory', function ($q, RESTbase, ELASTICprop, esClient, esFactory) {
    
    var get = function(id){
      var def=$q.defer();

      esClient.get({
          'index': ELASTICprop.index,
          'type': ELASTICprop.type,
          'id': id
        }).then(function (data) {
                def.resolve(data['_source']);
        });

      return def.promise;
    };

    // Public API here
    return {
      get: get
    };
  }]);
