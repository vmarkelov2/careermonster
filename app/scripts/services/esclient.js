'use strict';

/**
 * @ngdoc service
 * @name careerMonsterApp.esClient
 * @description
 * # esClient
 * Service in the careerMonsterApp.
 */
angular.module('careerMonsterApp')
  .service('esClient', ['esFactory', 'RESTbase', 'ELASTICprop', function esClient(esFactory, RESTbase, ELASTICprop) {
    return esFactory({
        host: RESTbase,
        apiVersion: '1.2'
      });
  }]);
