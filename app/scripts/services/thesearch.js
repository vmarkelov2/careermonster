'use strict';

/**
 * @ngdoc service
 * @name careerMonsterApp.TheSearch
 * @description
 * # TheSearch
 * Factory in the careerMonsterApp.
 */
angular.module('careerMonsterApp')
  .factory('TheSearch', ['$q', 'RESTbase', 'PAGINATION', 'ELASTICprop', 'esClient', 'esFactory' , 'fullTextFields', function ($q, RESTbase, PAGINATION, ELASTICprop, esClient, esFactory, fullTextFields) {
    // Service logic
    var canceler = null;

    var searchFor = function (str, options, pageNum) {
      var def=$q.defer();
      if(canceler) {
        canceler.reject();
      }
      canceler = def;
      
      var filter = [];

      options.forEach(function(index){
        var variantsSet = index.variants.filter(function(i){
          return i.selected;
        }).map(function(i){
          return ''+i.key+'';
        });
        if (variantsSet.length) {
          var term = 
          {"query":{
                  "multi_match":{
                     "fields": index.name,
                     "query": variantsSet.join(' ')
                  }
               }}
          
          filter.push(term);
        }
      });

      var quryBody = { 
                  'query':{
                    'filtered': {
                      'query': {
                        'match': {
                          '_all': str
                        }
                      }
                    }
                  }
                };
      if(filter.length){
        quryBody.query.filtered.filter={'and': filter}
      }                
      esClient.search({
          'index': ELASTICprop.index,
          'type': ELASTICprop.type,
          'from': pageNum*PAGINATION.size,
          'size': PAGINATION.size,
          'body': quryBody
        }).then(function (resp) {
            canceler = null;
            var vacancies = resp['hits']['hits'].map(function(item){
              return angular.extend(item['_source'],{
                  'id': item['_id']
                });
              });
            def.resolve({
              'v': vacancies, 
              't': resp['hits']['total']
            });
        }, function (err) {
            console.trace(err.message);
            def.reject();
        });

      return def.promise;
    };

    // Public API here
    return {
      searchFor: searchFor
    };
  }]);
