'use strict';

/**
 * @ngdoc service
 * @name careerMonsterApp.fullTextFields
 * @description
 * # fullTextFields
 * Constant in the careerMonsterApp.
 */
angular.module('careerMonsterApp')
  .constant('fullTextFields', ['jobtitle','description']);
