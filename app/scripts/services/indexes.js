'use strict';

/**
 * @ngdoc service
 * @name careerMonsterApp.Indexes
 * @description
 * # Indexes
 * Factory in the careerMonsterApp.
 */
angular.module('careerMonsterApp')
  .factory('Indexes', ['RESTbase', 'ELASTICprop', '$q', '$http', '$location', 'esClient', 'esFactory', 'fullTextFields', function (RESTbase, ELASTICprop, $q, $http, $location, esClient, esFactory, fullTextFields) {
    // Service logic
    var URL = RESTbase + '/_mapping',
        cache = null,
        ignoreList = fullTextFields; 


    var getOptions = function (contextString) {
      var def=$q.defer(),
          sparam = Object.keys($location.search()).filter(function(i){return i!='m' && i!='p';});

      // if(cache){
      //   def.resolve(cache);
      //   return def.promise;
      // }
      
      $http.get(URL).success(function (data) {
        var options = Object.keys(data[ELASTICprop['index']]['mappings'][ELASTICprop['type']]['properties']).filter(function(i){
          return ignoreList.indexOf(i)<0;
        });

        var aggs = options.reduce(function(memo, option){
          memo[option]={"terms" : { "field" : option+'.untouched', "size": 1000 }};
          return memo;
        },{});

        var searchObject = {
          'index': ELASTICprop.index,
          'type': ELASTICprop.type,
          'size': 0,
          'body': {
            "aggs": aggs
          }
        };
        if(contextString) searchObject.q=contextString;
        esClient.search(searchObject).then(function (resp) {
              cache = [];
              angular.forEach(resp["aggregations"], function (data, key) {
                        cache.push({
                          name: key, 
                          variants: data["buckets"].map(function(i){
                            return {
                                selected: sparam.indexOf(i.key.toLowerCase())>-1,
                                key: i.key.toLowerCase(),
                                doc_count: i.doc_count
                            };
                          }).sort(function(a,b){
                              if(a.key<b.key) return -1;
                              if(a.key>b.key) return 1;
                              return 0;
                          }).filter(function(value, i, arr){
                            if(i>0){
                              if(value.key == arr[i-1].key){
                                arr[i-1].doc_count+=value.doc_count;
                                return false;
                              }
                            } return true;
                          })
                        });
            });
            def.resolve(cache);

        }, function (err) {
            console.trace(err.message);
            def.reject();
        });
      });

      return def.promise;
    };

    // Public API here
    return {
      getOptions: getOptions
    };
  }]);
