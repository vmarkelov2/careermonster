'use strict';

angular.module('careerMonsterApp')
  .constant('RESTbase', 'https://seb.east-us.azr.facetflow.io')
  .constant('ELASTICprop', {'index': 'test_vacancies_v2', 'type': 'vacancy'})
  .constant('PAGINATION', {'size': 10});
