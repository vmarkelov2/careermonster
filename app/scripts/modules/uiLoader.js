// Use directive LOADER to assign some UI element to be overlaped by loadmask in case rlated AJAX request is in progress.
//Example:
//HTML <div loader="some-id-to-track-from-ajax-requests-10">blablalblalbla data from server</div>
//JS $http.get('/somerestservice/entity/10', { headers: {'requesting': 'some-id-to-track-from-ajax-requests-10'}}).success(function ( ......


angular.module('uiLoader', [])
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push('loadingStatusInterceptor');
        }])
        .factory('loadingStatusInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
            var activeRequests = 0;
            var started = function (requesting) {
                if (requesting === 'main') {
                    if (activeRequests === 0) {
                        $rootScope.$broadcast('loadingStatusActive', requesting);
                    }
                    activeRequests++;
                }
                else {
                    $rootScope.$broadcast('loadingStatusActive', requesting);
                }
            };
            var ended = function (stopRequesting) {
                if (stopRequesting === 'main') {
                    activeRequests--;
                    if (activeRequests === 0) {
                        $rootScope.$broadcast('loadingStatusInactive', stopRequesting);
                    }
                }
                else {
                    $rootScope.$broadcast('loadingStatusInactive', stopRequesting);
                }
            };
            return {
                request: function (config) {
                    var requesting = 'main';
                    if (typeof config.headers.requesting !== 'undefined') {
                        requesting = config.headers.requesting;
                    }
                    started(requesting);
                    return config || $q.when(config);
                },
                response: function (response) {
                    var stopRequesting = 'main';
                    if (typeof response.config.headers.requesting !== 'undefined') {
                        stopRequesting = response.config.headers.requesting;
                    }
                    ended(stopRequesting);
                    return response || $q.when(response);
                },
                responseError: function (rejection) {
                    var stopRequesting = 'main';
                    if (typeof rejection.config.headers.requesting !== 'undefined') {
                        stopRequesting = rejection.config.headers.requesting;
                    }
                    ended(stopRequesting);
                    return $q.reject(rejection);
                }
            };
        }])
        .directive('loader', function () {
            return {
                scope: {
                    loader: '@'
                },
                link: function ($scope, $element) {
                    var $loader = $('<div />')
                            .css({'maxWidth': '200px', 'maxHeight': '200px'})
                            .html('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%"><g><rect x="10%" y="35%" width="20%" height="20%" style="opacity:0.3"><animate attributeType="CSS" attributeName="opacity" from="0.3" to="0" dur="1.2s" repeatCount="indefinite" begin="0s"/></rect><rect x="40%" y="35%" width="20%" height="20%" style="opacity:0.3"><animate attributeType="CSS" attributeName="opacity" from="0.3" to="0" dur="1.2s" repeatCount="indefinite" begin="0.1s"/></rect><rect x="70%" y="35%" width="20%" height="20%" style="opacity:0.3"><animate attributeType="CSS" attributeName="opacity" from="0.3" to="0" dur="1.2s" repeatCount="indefinite" begin="0.2s"/></rect></g></svg>');
                    $element.prepend($loader);

                    var show = function (e, d) {
                        if (d === $scope.loader) {
                            $loader.show();
                        }
                    };
                    var hide = function (e, d) {
                        if (d === $scope.loader) {
                            $loader.hide();
                        }
                    };
                    $scope.$on('loadingStatusActive', show);
                    $scope.$on('loadingStatusInactive', hide);
                    show({}, $scope.loader);
                }
            };
        });