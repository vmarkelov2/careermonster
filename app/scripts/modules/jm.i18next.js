'use strict';

// Translation module
angular.module('jm.i18next').config(['$i18nextProvider', function ($i18nextProvider)
{
    var country = "NL";

    $i18nextProvider.options = {
        debug: true,
        lng: 'en',
        fallbackLng: false,
        useCookie: false,
        useLocalStorage: false,
        resGetPath: '../locales/' + country + '/__ns__.__lng__.json',
        ns: {
            namespaces: ['messages','indexes'],
            defaultNs: 'messages'
        },
        getAsync: false
    };
}]);

