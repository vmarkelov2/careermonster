'use strict';

angular.module('careerMonsterApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'jm.i18next',
    'uiLoader',
    'ui.bootstrap',
    'elasticsearch'
  ])
  .config(['$routeProvider', '$provide', function ($routeProvider, $provide) {
    $routeProvider.
            when('/creers', { 
                templateUrl: 'views/_main.html', 
                controller: 'MainCtrl',
                resolve: {
                  options: ['Indexes', function(Indexes){
                    return Indexes.getOptions().then(function(options){return options;});
                  }]
                },
                reloadOnSearch: false
            }).
            when('/vacancy/:id', { 
                templateUrl: 'views/_vacancy.html', 
                controller: 'VacancyCtrl',
                resolve: {
                  options: ['Indexes', function(Indexes){
                    return Indexes.getOptions().then(function(options){return options;});
                  }]
                }
            }).
            otherwise({ redirectTo: '/creers' });
  }])
  .run(['$rootScope', '$location', function ($rootScope, $location) {

  }]);
