'use strict';

/**
 * @ngdoc directive
 * @name careerMonsterApp.directive:pagination
 * @description
 * # pagination
 */
angular.module('careerMonsterApp')
  .directive('paginationui', function () {
    return {
      template: '<ul class="pagination" ng-show="numberOfPages()>1?true:false">'+
			  		'<li ng-repeat="pn in [] | range:numberOfPages()" ng-class="{\'active\': getCurrentPage() == pn}">'+
			  		'	<a ng-click="setPage(pn)" href="">{{pn+1}}</a>'+
			  		'</li>'+
				'</ul>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

      }
    };
  });
