'use strict';

/**
 * @ngdoc directive
 * @name careerMonsterApp.directive:oneoption
 * @description
 * # oneoption
 */
angular.module('careerMonsterApp')
  .directive('oneoption', function () {
    return {
      template: '<span class="label label-info label-option" ng-class="{\'selected\': item.selected}" ng-click="item.selected=!item.selected">'+
					'{{item.key}} <span class="badge">{{item.doc_count}}</span>'+
				'</span>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });
