'use strict';

angular.module('careerMonsterApp')
  .filter('capitalize', function () {
    return function (input) {
        if (input !== null) {
          input = input.toLowerCase();
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    };
});

angular.module('careerMonsterApp')
  .filter('shorten', function () {
    return function (input) {
        var res=input;  
        if(input == null) return '';
        if(input.length>50){
          res=input.substring(0, 50)+'...'
        }
        return res;
    };
});

angular.module('careerMonsterApp')
  .filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

angular.module('careerMonsterApp')
  .filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});