'use strict';

/**
 * @ngdoc function
 * @name careerMonsterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the careerMonsterApp
 */
angular.module('careerMonsterApp')
  .controller('MainCtrl', ['$http', '$q', '$location', 'RESTbase', 'Indexes', '$scope', 'TheSearch', 'options', 'PAGINATION', function ($http, $q, $location, RESTbase, Indexes, $scope, TheSearch, options, PAGINATION) {
  	
    var getMainSearchTerm = function () {
      var searchParams = $location.search();
        if(searchParams.hasOwnProperty('m')) {
          return searchParams['m'];
        }
      return '';
    };

    var setMainSearchTerm = function(text) {
      if(text!=''){
        var searchParams = $location.search();
        angular.extend(searchParams,{'m': text});
        $location.search(searchParams);
      } else {
        $location.search({});
      }
    };

    var setOptionsToURL = function(insertOptionName){
      var searchParams = $location.search();
      var newSearchParams = {};
      if(searchParams['m']) newSearchParams['m']=searchParams['m'];
      if($scope.options.length){
        $scope.options.forEach(function(index){
          index.variants.forEach(function(variant){
            if(insertOptionName){
              // variant.selected |= insertOptionName.split(' ').reduce(function(memo, i){
              //   return memo || ( variant.key.toLowerCase() == i.toLowerCase() );
              // }, false);
              variant.selected |= variant.key.toLowerCase()==insertOptionName.toLowerCase();
            }
            if(variant.selected) {
              newSearchParams[variant.key]=true;
            }
          })
        });
      $location.search(newSearchParams);
      }
    };

    $scope.setOptionsToURL = setOptionsToURL;
    $scope.searchTerm = getMainSearchTerm();
    $scope.pristine=$scope.searchTerm?false:true;
    $scope.vacancies = [];
    $scope.options = options;
    $scope.currentPage = 0;
    $scope.pageCount = 1;
    $scope.pageSize = PAGINATION.size;

    $scope.numberOfPages=function(){
      return $scope.pageCount;                
    }

    $scope.setPage = function(n){
      $scope.currentPage = n;
      $scope.search();
    };

    $scope.getCurrentPage = function(n){
      return $scope.currentPage;
    };

    $scope.find = function(){

      setOptionsToURL();
      setMainSearchTerm($scope.searchTerm);
    }

    $scope.search = function(){
      var text = $scope.searchTerm = getMainSearchTerm();
    	$scope.pristine = text!=''?false:true;
      if(text!=''){
      	TheSearch.searchFor(text,$scope.options,$scope.currentPage).then(function(data){
      		$scope.vacancies=data.v;
          $scope.pageCount = Math.ceil(data.t/PAGINATION.size);
      	});
        Indexes.getOptions(text).then(function(data){$scope.options = data;});
      } 
    };

    $scope.$watch(function(){ 
    	return $location.search();
     }, function(newValue, oldValue){
       $scope.currentPage = 0;
  		$scope.search();
	   }, true);

    $scope.$watch('options', function(newValue, oldValue){
      setOptionsToURL();
     }, true); 

  }]);
