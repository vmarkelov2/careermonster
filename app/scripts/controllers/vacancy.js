'use strict';

/**
 * @ngdoc function
 * @name careerMonsterApp.controller:VacancyCtrl
 * @description
 * # VacancyCtrl
 * Controller of the careerMonsterApp
 */
angular.module('careerMonsterApp')
  .controller('VacancyCtrl', ['$scope', 'Vacancies', '$routeParams', 'options', function ($scope, Vacancies, $routeParams, options) {
    $scope.vacancy={
    	jobtitle: '',
    	description: ''
    };
    $scope.options = options;
    Vacancies.get($routeParams.id).then(function(data){
    	$scope.vacancy = data;
    });
  }]);
