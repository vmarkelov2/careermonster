'use strict';

/**
 * @ngdoc function
 * @name careerMonsterApp.controller:SearchoptionCtrl
 * @description
 * # SearchoptionCtrl
 * Controller of the careerMonsterApp
 */
angular.module('careerMonsterApp')
  .controller('SearchoptionCtrl', ['$scope', '$modal', function ($scope, $modal) {

	$scope.selected = function(item){
		return item.selected;
	};

	$scope.showMore = function (option) {
	    var modalInstance = $modal.open({
	      templateUrl: 'views/_moreOptions.html',
	      controller: ['$scope', '$modalInstance', 'optionData', function ($scope, $modalInstance, optionData) {
	      	  $scope.option=optionData;	
			  $scope.ok = function () {
			    $modalInstance.close($scope.option);
			  };
			  $scope.cancel = function () {
			    $modalInstance.dismiss('cancel');
			  };
			}],
	      resolve: {
	        optionData: function(){ return angular.copy(option);}
	        }
	    });

	    modalInstance.result.then(function (result) {
	      angular.extend(option, result);

	    }, function () { //modal dismiss
	    });
	  };

  }]);
